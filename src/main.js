
import jquery from 'jquery'
import 'jquery.nicescroll'
import Vue from 'vue'
import Router from 'vue-router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import Axios from 'axios'
import md5 from 'js-md5';
import VueCookies from 'vue-cookies'
import Moment from 'moment'

Vue.prototype.$moment = Moment
Vue.prototype.$md5 = md5
Vue.prototype.$axios = Axios
Vue.prototype.$jquery = jquery
Vue.prototype.$eventBus = new Vue();
window.$eventBus = Vue.prototype.$eventBus;

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const eventBusMap = new Map();
Vue.prototype.utils = {
  hasRight(target) {
    if (!target || !Vue.prototype.config.user.rights) {
      return false;
    }

    return Vue.prototype.config.user.rights.indexOf(target) != -1;
  },
  cleanUserData() {
    Vue.prototype.$cookies.set("token", "");
  },
  http: function (type, url, data, success, error) {
    let tempaxios = type == 'get' ? Vue.prototype.$axios.get : Vue.prototype.$axios.post;
    let params = {};
    if (type == 'get') {
      tempaxios = Vue.prototype.$axios.get;
      params = { params: data };
    } else {
      tempaxios = Vue.prototype.$axios.post;
      params = data;
    }

    const token = Vue.prototype.$cookies.get("token");
    if (url.indexOf('?') == -1) {
      url = url + "?__id=" + new Date().getTime() + "&token=" + token;
    } else {
      url = url + "&__id=" + new Date().getTime() + "&token=" + token;
    }

    tempaxios(url, params).then(resp => {
      if (resp.data.code && (resp.data.code == '1' || resp.data.code == '2')) {
        Vue.prototype.utils.emit("page_go_login");
        return;
      }

      if (!resp.data.success) {
        if (error) {
          error(resp.data.message);
          return;
        }
        Vue.prototype.$notify({
          title: "错误",
          message: resp.data.message,
          type: 'error'
        });
        return;
      }

      if (success) {
        success(resp.data.content);
      }
    }).catch(() => {
      Vue.prototype.$notify({
        title: "错误",
        message: '请求网络失败，请检测网络是否正常',
        type: 'error'
      });
    });
  },
  refreshRootContentHeight: function () {
    Vue.prototype.$jquery(".root-content").css("height", `${Vue.prototype.config.height - 65}px`);//65
  },
  /** 总线增强 start*/
  on: function (uid, key, callback) {
    const tempkey = key + '-' + uid;
    let list = eventBusMap.get(key);
    if (!list) {
      list = [];
      eventBusMap.set(key, list);
    }
    list.push(tempkey);
    Vue.prototype.$eventBus.$on(tempkey, callback);
  },
  unon: function (uid, key) {
    const tempkey = key + '-' + uid;
    let list = eventBusMap.get(key);
    if (!list) {
      return;
    }

    Vue.prototype.$eventBus.$off(tempkey);
    list.splice(list.findIndex(e => e === tempkey), 1);
  },
  emit: function (key, data) {
    let list = eventBusMap.get(key);
    if (!list) {
      return;
    }
    for (const item of list) {
      Vue.prototype.$eventBus.$emit(item, data);
    }
  }
  /** 总线增强 end*/
}

Vue.prototype.config = {
  copyright: 'V2.0.0 百百堡垒机',
  height: 0,
  user: {},
  rights: {
    team_user_manager: false,
    team_role_manager: false,
    device_remotecpe: false,
    device_node_select: false,
    device_node_manager: false,
    device_device_select: false,
    device_device_manager: false,
    browser_audit_video_manger: false,
    browser_audit_video_list: false,
  },
}

window.onresize = () => {
  const height = document.body.clientHeight;
  const width = document.body.clientWidth;
  Vue.prototype.config.height = height;
  Vue.prototype.config.width = width;
  Vue.prototype.utils.refreshRootContentHeight();
  Vue.prototype.utils.emit('refresh_height', height);
  Vue.prototype.utils.emit('refresh_client_wh', { width, height });
};

Vue.use(VueCookies)
Vue.use(Router)
Vue.use(ElementUI)

const constantRoutes = [
  {
    path: '/loading',
    component: () => import('~/views/loading/index'),
    hidden: true
  },
  {
    path: '/init',
    component: () => import('~/views/init/index'),
    hidden: true
  },
  {
    path: '/login',
    component: () => import('~/views/login/index'),
    hidden: true
  },
  {
    path: '/basic',
    component: () => import('~/views/basic/index'),
    hidden: true
  },
  {
    path: '/device',
    component: () => import('~/views/device/index'),
    hidden: true
  },
  {
    path: '/team',
    component: () => import('~/views/team/index'),
    hidden: true
  },
  {
    path: '/audit',
    component: () => import('~/views/audit/index'),
    hidden: true
  },
  {
    path: '/self',
    component: () => import('~/views/self/index'),
    hidden: true
  },
]

const createRouter = () => new Router({
  routes: constantRoutes
})

const router = createRouter()

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
